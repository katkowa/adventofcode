import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/***
 * --- Day 3: Crossed Wires ---
 * The gravity assist was successful, and you're well on your way to the Venus refuelling station. During the rush back on Earth, the fuel management system wasn't completely installed, so that's next on the priority list.
 *
 * Opening the front panel reveals a jumble of wires. Specifically, two wires are connected to a central port and extend outward on a grid. You trace the path each wire takes as it leaves the central port, one wire per line of text (your puzzle input).
 *
 * The wires twist and turn, but the two wires occasionally cross paths. To fix the circuit, you need to find the intersection point closest to the central port. Because the wires are on a grid, use the Manhattan distance for this measurement. While the wires do technically cross right at the central port where they both start, this point does not count, nor does a wire count as crossing with itself.
 *
 * For example, if the first wire's path is R8,U5,L5,D3, then starting from the central port (o), it goes right 8, up 5, left 5, and finally down 3:
 *
 * ...........
 * ...........
 * ...........
 * ....+----+.
 * ....|....|.
 * ....|....|.
 * ....|....|.
 * .........|.
 * .o-------+.
 * ...........
 * Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6, down 4, and left 4:
 *
 * ...........
 * .+-----+...
 * .|.....|...
 * .|..+--X-+.
 * .|..|..|.|.
 * .|.-X--+.|.
 * .|..|....|.
 * .|.......|.
 * .o-------+.
 * ...........
 * These wires cross at two locations (marked X), but the lower-left one is closer to the central port: its distance is 3 + 3 = 6.
 *
 * Here are a few more examples:
 *
 * R75,D30,R83,U83,L12,D49,R71,U7,L72
 * U62,R66,U55,R34,D71,R55,D58,R83 = distance 159
 * R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
 * U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135
 * What is the Manhattan distance from the central port to the closest intersection?
 *
 * Your puzzle answer was 865.
 *
 * --- Part Two ---
 * It turns out that this circuit is very timing-sensitive; you actually need to minimize the signal delay.
 *
 * To do this, calculate the number of steps each wire takes to reach each intersection; choose the intersection where the sum of both wires' steps is lowest. If a wire visits a position on the grid multiple times, use the steps value from the first time it visits that position when calculating the total value of a specific intersection.
 *
 * The number of steps a wire takes is the total number of grid squares the wire has entered to get to that location, including the intersection being considered. Again consider the example from above:
 *
 * ...........
 * .+-----+...
 * .|.....|...
 * .|..+--X-+.
 * .|..|..|.|.
 * .|.-X--+.|.
 * .|..|....|.
 * .|.......|.
 * .o-------+.
 * ...........
 * In the above example, the intersection closest to the central port is reached after 8+5+5+2 = 20 steps by the first wire and 7+6+4+3 = 20 steps by the second wire for a total of 20+20 = 40 steps.
 *
 * However, the top-right intersection is better: the first wire takes only 8+5+2 = 15 and the second wire takes only 7+6+2 = 15, a total of 15+15 = 30 steps.
 *
 * Here are the best steps for the extra examples from above:
 *
 * R75,D30,R83,U83,L12,D49,R71,U7,L72
 * U62,R66,U55,R34,D71,R55,D58,R83 = 610 steps
 * R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51
 * U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = 410 steps
 * What is the fewest combined steps the wires must take to reach an intersection?
 *
 * Your puzzle answer was 35038.
 */
public class CrossedWires {

    public static void main(String[] args) {
        CrossedWires unit = new CrossedWires();
        BufferedReader reader = new BufferedReader(new InputStreamReader(unit.getClass().getResourceAsStream("/crossed-wires.txt")));
        try {
            String[] path1 = reader.readLine().split(",");
            String[] path2 = reader.readLine().split(",");
            List<Point> pointList1 = unit.getPointList(path1);
            List<Point> pointList2 = unit.getPointList(path2);
            List<Point> crosses = unit.getCrosses(pointList1, pointList2);
            System.out.println(unit.findClosestCross(crosses));
            System.out.println(unit.findCrossWithMinSteps(pointList1, pointList2, crosses));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public int getDistanceToCrossed(String[] path1, String[] path2) {
        List<Point> pointList1 = getPointList(path1);
        List<Point> pointList2 = getPointList(path2);
        List<Point> crosses = getCrosses(pointList1, pointList2);
        return findClosestCross(crosses);
    }

    public int getMinSteps(String[] path1, String[] path2) {
        List<Point> pointList1 = getPointList(path1);
        List<Point> pointList2 = getPointList(path2);
        List<Point> crosses = getCrosses(pointList1, pointList2);
        return findCrossWithMinSteps(pointList1, pointList2, crosses);
    }

    private List<Point> getPointList(String[] path) {
        List<Point> pointList = new ArrayList<>();
        Point point = new Point(0,0);
        for (String instruction: path) {
            String direction = instruction.substring(0,1);
            int value = Integer.parseInt(instruction.substring(1));
            for (int i = 1; i <= value; i++) {
                point = move(point, direction, 1);
                if (!point.equals(new Point(0,0)))
                pointList.add(point);
            }
        }
        return pointList;
    }

    private Point move(Point point, String direction, int value) {
        switch (direction) {
            case "U":
                return new Point(point.x, point.y + value);
            case "R":
                return new Point(point.x + value, point.y);
            case "D":
                return new Point(point.x, point.y - value);
            case "L":
                return new Point(point.x - value, point.y);
        }
        return null;
    }

    private List<Point> getCrosses(List<Point> pointList1, List<Point> pointList2) {
        List<Point> crosses = new ArrayList<>();
        for (Point point1: pointList1) {
            for (Point point2: pointList2) {
                if (point1.x == point2.x && point1.y == point2.y) {
                    crosses.add(point1);
                }
            }
        }
        return crosses;
    }

    private int findCrossWithMinSteps(List<Point> pointList1, List<Point> pointList2, List<Point> crosses) {
        List<Integer> steps = new ArrayList<>();
        for (Point crossPoint : crosses) {
            steps.add(pointList1.indexOf(crossPoint) + pointList2.indexOf(crossPoint) + 2);
        }
        Collections.sort(steps);
        return steps.get(0);
    }

    private int findClosestCross(List<Point> crosses) {
        List<Integer> distances = new ArrayList<>();
        crosses.forEach(point -> {
            int distance = Math.abs(point.y) + Math.abs(point.x);
            distances.add(distance);
        });
        Collections.sort(distances);
        return distances.get(0);
    }
}
