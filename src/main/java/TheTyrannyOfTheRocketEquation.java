import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/***
 * --- Day 1: The Tyranny of the Rocket Equation ---
 * Santa has become stranded at the edge of the Solar System while delivering presents to other planets! To accurately
 * calculate his position in space, safely align his warp drive, and return to Earth in time to save Christmas,
 * he needs you to bring him measurements from fifty stars.
 *
 * Collect stars by solving puzzles. Two puzzles will be made available on each day in the Advent calendar;
 * the second puzzle is unlocked when you complete the first. Each puzzle grants one star. Good luck!
 *
 * The Elves quickly load you into a spacecraft and prepare to launch.
 *
 * At the first Go / No Go poll, every Elf is Go until the Fuel Counter-Upper. They haven't determined the amount of fuel required yet.
 *
 * Fuel required to launch a given module is based on its mass. Specifically, to find the fuel required for a module,
 * take its mass, divide by three, round down, and subtract 2.
 *
 * For example:
 *
 * For a mass of 12, divide by 3 and round down to get 4, then subtract 2 to get 2.
 * For a mass of 14, dividing by 3 and rounding down still yields 4, so the fuel required is also 2.
 * For a mass of 1969, the fuel required is 654.
 * For a mass of 100756, the fuel required is 33583.
 * The Fuel Counter-Upper needs to know the total fuel requirement. To find it, individually calculate the fuel needed for the mass of each module (your puzzle input), then add together all the fuel values.
 *
 * What is the sum of the fuel requirements for all of the modules on your spacecraft?
 */
public class TheTyrannyOfTheRocketEquation {
    private static TheTyrannyOfTheRocketEquation module = new TheTyrannyOfTheRocketEquation();

    public static void main(String[] args) {
        List<Integer> massList = module.getData();
        System.out.println(module.getFuelForModules(massList));
        System.out.println(module.getTotalFuelForModules(massList));
    }

    private List<Integer> getData() {
        try {
            List<Integer> list = new ArrayList<>();
            BufferedReader br = new BufferedReader(new FileReader(getClass().getResource("day1.txt").getFile()));
            String line;
            while ((line = br.readLine()) != null) {
                list.add(Integer.parseInt(line));
            }
            return list;
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }

    public int getFuel(int mass) {
        return (mass / 3) - 2;
    }

    private int getFuelForModules(List<Integer> fuelList) {
        return fuelList.stream().map(module::getFuel).mapToInt(Integer::intValue).sum();
    }

    public int getTotalFuel(int mass) {
        int fuel = getFuel(mass);
        int result = 0;
        while (fuel > 0) {
            result += fuel;
            fuel = getFuel(fuel);
        }

        return result;
    }

    private int getTotalFuelForModules(List<Integer> inputData) {
        return inputData.stream().map(module::getTotalFuel).mapToInt(Integer::intValue).sum();
    }
}
