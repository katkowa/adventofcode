import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class SecureContainerTest {
    private final SecureContainer unit = new SecureContainer();

    @Test
    public void isNumberValidTest() {
        assertThat(unit.isNumberValid(122345), is(true));
        assertThat(unit.isNumberValid(111111), is(false));
        assertThat(unit.isNumberValid(223450), is(false));
        assertThat(unit.isNumberValid(123789), is(false));
        assertThat(unit.isNumberValid(112233), is(true));
        assertThat(unit.isNumberValid(123444), is(false));
        assertThat(unit.isNumberValid(111122), is(true));
        assertThat(unit.isNumberValid(223335), is(true));
    }
}