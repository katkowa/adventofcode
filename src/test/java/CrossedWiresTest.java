import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class CrossedWiresTest {
    @Parameterized.Parameter
    public String[] pathOne;
    @Parameterized.Parameter(value = 1)
    public String[] pathTwo;
    @Parameterized.Parameter(value = 2)
    public int distance;
    @Parameterized.Parameter(value = 3)
    public int steps;

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {new String[]{"R8", "U5", "L5", "D3"}, new String[]{"U7", "R6", "D4", "L4"}, 6, 30},
                {new String[]{"R75", "D30", "R83", "U83", "L12", "D49", "R71", "U7", "L72"},
                        new String[]{"U62", "R66", "U55", "R34", "D71", "R55", "D58", "R83"}, 159, 610},
                {new String[]{"R98", "U47", "R26", "D63", "R33", "U87", "L62", "D20", "R33", "U53", "R51"},
                        new String[]{"U98", "R91", "D20", "R16", "D67", "R40", "U7", "R15", "U6", "R7"}, 135, 410},
        });
    }

    @Test
    public void getDistanceToCrossedTest() {
        CrossedWires unit = new CrossedWires();
        assertThat(unit.getDistanceToCrossed(pathOne, pathTwo), is(distance));
    }

    @Test
    public void getMinStepsTest() {
        CrossedWires unit = new CrossedWires();
        assertThat(unit.getMinSteps(pathOne, pathTwo), is(steps));
    }
}