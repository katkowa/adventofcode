import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(Parameterized.class)
public class TheTyrannyOfTheRocketEquationTest {
    public static TheTyrannyOfTheRocketEquation unitUnderTest;

    @Parameterized.Parameter
    public int mass;
    @Parameterized.Parameter(value = 1)
    public int fuel;
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {12, 2}, {14, 2}, {1969, 654}, {100756, 33583}
        });
    }

    @BeforeClass
    public static void init() {
        unitUnderTest = new TheTyrannyOfTheRocketEquation();
    }

    @Test
    public void testGetFuel() {
        assertThat(unitUnderTest.getFuel(mass), is(fuel));
    }

    @Test
    public void testGetTotalFuel() {
        assertThat(unitUnderTest.getTotalFuel(14), is(2));
        assertThat(unitUnderTest.getTotalFuel(1969), is(966));
        assertThat(unitUnderTest.getTotalFuel(100756), is(50346));
    }
}