import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class ProgramAlarmTest {
    @Parameterized.Parameter
    public int[] input;
    @Parameterized.Parameter(value = 1)
    public int[] output;
    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                {new int[] {1,0,0,0,99}, new int[] {2,0,0,0,99}},
                {new int[] {2,3,0,3,99}, new int[] {2,3,0,6,99}},
                {new int[] {2,4,4,5,99,0}, new int[] {2,4,4,5,99,9801}},
                {new int[] {1,1,1,4,99,5,6,0,99}, new int[] {30,1,1,4,2,5,6,0,99}},
                {new int[] {1,9,10,3,2,3,11,0,99,30,40,50}, new int[] {3500,9,10,70,2,3,11,0,99,30,40,50}}
        });
    }

    @Test
    public void getIntcode() {
        ProgramAlarm unit = new ProgramAlarm();
        assertThat(unit.getIntcode(input), is(output));
    }
}